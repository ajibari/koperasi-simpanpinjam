<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

$library_dir = dirname(__FILE__);
require_once($library_dir . '/Html2pdf/vendor/autoload.php');
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Htmltopdf {

	/**
	 * CI instance
	 *
	 * @var CodeIgniter Super Controller Object
	 */
	protected $CI;
	protected $library_dir;
	public $html2pdf;
	
	/**
	 * Create FineUploader instace.
	 *
	 * @param array $properties
	 * @return void
	 */
	function __construct($properties = array())
	{

		// Load additional libraries, helpers, etc.
		$this->CI =& get_instance();

	}

	function fill_data($arr){
		if(!isset($arr['orientation'])){
			$arr['orientation'] = 'P';
		}
		if(!isset($arr['paper_size'])){
			$arr['paper_size'] = 'A4';
		}
		if(!isset($arr['lang'])){
			$arr['lang'] = 'en';
		}
		if(!isset($arr['download'])){
			$arr['download'] = false;
		}
		if(!isset($arr['margin'])){
			$arr['margin'] = null;
		}
		return $arr;	
	}

	function generate($arr){
		/**
			$arr = array('html_file', 'orientation', 'paper_size', 'lang', 'margin','filename', 'download', 'save_to_server');
		*/
		$arr = $this->fill_data($arr);
		ob_start();
	    include $arr['html_file'];
	    $content = ob_get_clean();

	    $html2pdf = new Html2Pdf($arr['orientation'], $arr['paper_size'], $arr['lang'], 'true', 'UTF-8', $arr['margin']);
	    $html2pdf->writeHTML($content);

	    if(isset($arr['save_to_server'])){
	    	$html2pdf->output($arr['filename'].'.pdf','F');
		}

		if($arr['download']){
	    	$html2pdf->output($arr['filename'].'.pdf','D');
	    	return true;
	    }

	    $html2pdf->output($arr['filename'].'.pdf');
	}

	function chunk_make($arr){
		// $content is an array

		$arr = $this->fill_data($arr);
		ob_start();
	    include $arr['html_file'];
	    $content = ob_get_clean();

	    $html2pdf = new Html2Pdf($arr['orientation'], $arr['paper_size'], $arr['lang'], 'true', 'UTF-8', $arr['margin']);
	    foreach($content as $c){
		    $html2pdf->writeHTML($c);
	    }

	    if(isset($arr['save_to_server'])){
	    	$html2pdf->output($arr['filename'].'.pdf','F');
		}

		if($arr['download']){
	    	$html2pdf->output($arr['filename'].'.pdf','D');
	    	return true;
	    }

	    $html2pdf->output($arr['filename'].'.pdf');
	}	

}
// End of library class
// Location: /libraries/html2pdf.php
