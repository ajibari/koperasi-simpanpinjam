<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * -----------------------------------------------------------------
 * General Library 
 * ----------------------------------------------------------------- 
 * Generic class, contains useful several method do common task in mc
 * application.
 *
 */

class General
{
    /**
     * Hold static $ci instance.
     *
     * @var Object
     */
    public $ci;

    /**
     * Create class instance.
     *
     * @param none
     * @return void
     */
	public function __construct()
	{
        $this->ci =& get_instance();
        $this->ci->load->helper('url');
        // $this->ci->load->model(array('dashboard/login_model'));
	}


	public function check_component($directory)
	{
		$this->ci->load->helper('directory');
		$map = directory_map("./mcapplication/modules/$directory/");
		
        if(!empty($map))
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}

    function array_uri()
    {
        $maxuri = $this->ci->uri->total_segments();
        $ret_uri = array();
        array_push($ret_uri, $maxuri);
        for($i=1; $i<= $maxuri; $i++){
        array_push($ret_uri, $this->ci->uri->segment($i));
        }
        return $ret_uri;
    }

    function array_data()
    {

        /*
        * Set Data for Header
        * -----------------------------------------------------
        */
        $uid = $this->ci->session->userdata("logged_uid");
        // $arr_login = $this->ci->login_model->get_info_userlogin($uid);

        /*
        * Load Menu base on database
        * -----------------------------------------------------
        */
        // $arr_menu = $this->ci->login_model->get_menu($uid);
        $arr_uri = $this->ci->general->array_uri();
        /*
        $data = array (
        'logininfo' => $arr_login,
        'menuinfo' => $arr_menu,
        'uriinfo' => $arr_uri
        );
        *
        */
        // $data['logininfo']= $arr_login;
        // $data['menuinfo'] = $arr_menu;
        // $data['uriinfo'] = $arr_uri;
        return $data;
    }
	
    //function to check whether user is disabled javascript
    public function is_js_disabled(){
        //play with js cookie
    	$this->ci->load->view('dashboard/js_disabled');
    }

  /**
   * Js console debugging. according to app envi.
   *
   * @param string $words
   * @return void
   */
    public function debug($words)
    {
        if (ENVIRONMENT === 'development')
            echo '<script type="text/javascript">console.log("'.$words.'");</script>';
    }

    public function array_searchs($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, $this->array_searchs($subarray, $key, $value));
            }
        }

        return $results;
    }

    public function generate_number(){
        //format belom ada
        return strtotime("now");
    }

    public function del_file($location, $filename){
        foreach($filename as $f){
            if(!unlink($location.$f['img'])){
                return false;
            }    
        }
        
        return true;
    }
}

/* End of file general.php */
/* Location: ./application/libraries/general.php */
