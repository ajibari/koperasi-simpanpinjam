<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Fineuploader {

	/**
	 * CI instance
	 *
	 * @var CodeIgniter Super Controller Object
	 */
	protected $CI;
	protected $library_dir;
	public $fineUploader;
	
	/**
	 * Create FineUploader instace.
	 *
	 * @param array $properties
	 * @return void
	 */
	function __construct($properties = array())
	{

		// Load additional libraries, helpers, etc.
		$this->CI =& get_instance();
		$this->library_dir = dirname(__FILE__);
		
	}

	function upload($input){
		
		//parameter: ext, size_limit, input_name, chunk_dir, upload_dir

		require_once($this->library_dir . '/FineUploader/handler.php');
		$this->fineUploader = new UploadHandler();

		// Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
			$this->fineUploader->allowedExtensions = !isset($input['ext'])?array():$input['ext'];
			//$this->fineUploader->allowedExtensions = array(); // all files types allowed by default

		// Specify max file size in bytes.
		// $this->fineUploader->sizeLimit = null;
			$this->fineUploader->sizeLimit = !isset($input['size_limit'])?null:$input['size_limit'];

		// Specify the input name set in the javascript.
		// $this->fineUploader->inputName = "qqfile"; // matches Fine Uploader's default inputName value by default
			$this->fineUploader->inputName = !isset($input['input_name'])?"qqfile":$input['input_name'];

		// If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
		// $this->fineUploader->chunksFolder = "chunks";
			$this->fineUploader->chunksFolder = !isset($input['chunk_dir'])?"chunks":$input['chunk_dir'];

		$method = $this->get_request_method();

		if ($method == "POST") {
		    header("Content-Type: text/plain");

		    // Assumes you have a chunking.success.endpoint set to point here with a query parameter of "done".
		    // For example: /myserver/handlers/endpoint.php?done
		    if (isset($_GET["done"])) {
		        $result = $this->fineUploader->combineChunks($input['chunk_dir']);
		    }
		    // Handles upload requests
		    else {
		        // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		        $result = $this->fineUploader->handleUpload($input['upload_dir']);

		        // To return a name used for uploaded file you can use the following line.
		        $result["uploadName"] = $this->fineUploader->getUploadName();
		    }

		    echo json_encode($result);
		}
		// for delete file requests
		else if ($method == "DELETE") {
		    $result = $this->fineUploader->handleDelete("files");
		    echo json_encode($result);
		}
		else {
		    header("HTTP/1.0 405 Method Not Allowed");
		}

	}

	function get_request_method() {
	    global $HTTP_RAW_POST_DATA;

	    if(isset($HTTP_RAW_POST_DATA)) {
	    	parse_str($HTTP_RAW_POST_DATA, $_POST);
	    }

	    if (isset($_POST["_method"]) && $_POST["_method"] != null) {
	        return $_POST["_method"];
	    }

	    return $_SERVER["REQUEST_METHOD"];
	}


}
// End of library class
// Location: /libraries/Authentication.php
