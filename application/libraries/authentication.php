<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Authentication {

	/**
	 * CI instance
	 *
	 * @var CodeIgniter Super Controller Object
	 */
	protected $CI;

	/**
	 * Index page to redirect user to.
	 *
	 * @var string
	 */
	protected $index_redirect = '';

	/**
	 * Login page to redirect user to.
	 *
	 * @var string
	 */
	protected $login_redirect = 'auth/login';

	
	/**
	 * Create Authentication instace.
	 *
	 * @param array $properties
	 * @return void
	 */
	function __construct($properties = array())
	{

		// Load additional libraries, helpers, etc.
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		$this->CI->load->database();
		$this->CI->load->helper('url');

		if (count($properties) > 0) $this->initialize($properties);
	}
	
	
	/**
	 * Initialize class preferences
	 *
	 * @access	public
	 * @param	array
	 * @return	void
	 */	
	function initialize($props = array())
	{
		if (count($props) > 0) 
		{
			foreach ($props as $key => $val)
			{
				$this->$key = $val;
			}
		}	
	}

	/**
	 * Proccess the login.
	 *
	 * @param array $login 
	 * @return bool
	 */
	function process_login( array $login = NULL)
	{
		// A few safety checks Our array has to be set
		if(!isset($login)) return FALSE;
			
		//Our array has to have 2 values No more, no less!
		if(count($login) != 2) return FALSE;
		
		$salt = 'ajibari__';
		$name = $login['username'];
		$password 	= password_hash($login['password'].$salt, PASSWORD_DEFAULT);

		$sql = "SELECT * FROM user_admin where username = '".$login['username']."' AND password = '".$password."' AND status = 'y'";
		$query = $this->CI->db->query($sql);

		if ($query->num_rows() == 1)
		    {
	        	// Our user exists, set session.
	        	$row = $query->row();
	        	
	        	$newdata = array(
		            'logged_uid' => $row->id,
		            'logged_user' => $row->username,
		            'logged_realname' => $row->first_name,
	         	);

	        	$this->CI->session->set_userdata($newdata);
	        	$log = array('user_id' => $row->id, 'aktifitas' => 'login','modul' => '', 'date_created' => date('Y-m-d'));
	       		$this->CI->activity->insert($log);
	        	
	        	return TRUE;
      		
      		}
	    
	        // No existing user.
    	    return FALSE;
     
	}
	
	/**
	 *
	 * This function redirects users after logging in
	 *
	 * @access	public
	 * @return	void
	 */		
	function redirect()
	{
		if ($this->CI->session->userdata('redirected_from') == FALSE)
		{
			redirect($this->index_redirect);
		}
		else
		{
			redirect($this->CI->session->userdata('redirected_from'));
		}
	}
	
	/**
	 *
	 * This function restricts users from certain pages.
	 * use restrict(TRUE) if a user can't access a page when logged in
	 *
	 * @access	public
	 * @param	boolean	wether the page is viewable when logged in
	 * @return	void
	 */	
	function restrict($logged_out = FALSE)
	{
		// If the user is logged in and he's trying to access a page
		// he's not allowed to see when logged in,
		// redirect him to the index!
		if ($logged_out && $this->logged_in())
		{
			redirect($this->index_redirect);
		}
		
		// If the user isn' logged in and he's trying to access a page
		// he's not allowed to see when logged out,
		// redirect him to the login page!
		if ( ! $logged_out && ! $this->logged_in()) 
		{
			$this->CI->session->set_userdata('redirected_from', $this->CI->uri->uri_string()); // We'll use this in our redirect method.
			redirect($this->login_redirect);
		}
	}
	
	/**
	 *
	 * Checks if a user is logged in
	 *
	 * @access	public
	 * @return	boolean
	 */	
	function logged_in()
	{
		if ($this->CI->session->userdata('logged_user') == FALSE)
		{
			return FALSE;
		}
		else 
		{
			return TRUE;
		}
	}

	/**
	 * Log the user out.
	 *
	 * @access	public
	 * @return	void
	 */	
	function logout($id) 
	{
		// expiring the cookie
		$cookie = array(
			'name'=> 'ajibari__',
			'value' => $id,
			'expire' => 0,
			'path' => '/',
			);

		$this->input->set_cookie($cookie);
		redirect(base_url());
	}

	/**
	 * Check if user is logged in.
	 *
	 * @access	public
	 * @return	void
	 */	
	function is_login()
	{
		$logged_uid = $this->CI->session->userdata('logged_uid');
		$logged_user = $this->CI->session->userdata('logged_user');
		$from_login_page = $this->CI->session->userdata('from_login_page');
	
		if(!empty($logged_uid)&&!empty($logged_user))
		{
			return TRUE;
		}
		else if($from_login_page){
			return FALSE;
		}
		else
		{
			//set session for prevent redirect forever
			$data = array('from_login_page' => true);
			$this->CI->session->set_userdata($data);
			echo "<script type=\"text/javascript\">
					window.location.href=\"".base_url()."auth/user\"
				 </script>";

			exit();
		}
	}


	/**
	 * 
	 *
	 * @access	public
	 * @return	void
	 */	
	function has_login()
	{
		$logged_uid = $this->CI->session->userdata('logged_uid');
		$logged_user = $this->CI->session->userdata('logged_user');
		if(!empty($logged_uid)&&!empty($logged_user))
		{
			return redirect(base_url());
		}
		else
		{
			return TRUE;
		}
	}

}
// End of library class
// Location: /libraries/Authentication.php
