<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		//kalo udah login redirect ke dashboard
		if(!$this->authentication->is_login()){
			$data['body_class'] = 'login';
			$data['title'] = 'Halaman Login';
			return $this->load->view('auth/login', $data, FALSE);	
		}
		else{
			redirect('dashboard/DashboardIndex/');
		}
		
	}
	
	public function get()
	{
		echo "get function";
	}
	
	public function add()
	{
		
	}
	
	public function edit()
	{
		
	}
	
	public function del()
	{
		
	}
	
	public function login(){
		
		if(!empty($this->input->post('login'))){
			$post = $this->input->post();
			// $login = array('username' => $post['username'], 'password' => $post['password']);
			// $this->authentication->process_login($login);
			$data = array('logged_user' => 1, 'logged_uid' => 2);
			$this->session->set_userdata($data);

			redirect(base_url().'dashboard/DashboardIndex');
		}
		else{
			redirect(base_url().'auth/user');
		}
	}

	public function forget_password(){
		if($this->authentication->is_login()){
			redirect('dashboard/DashboardIndex');
		}

		$data['body_class'] = 'login';
		$data['title'] = 'Halaman Login';
		return $this->load->view('auth/forget_password', $data, FALSE);	
	}

	public function process_update()
	{
			//penamaan process_ hanya untuk proses di backend tanpa view
	}

	private function _process_login($input){
		
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('dashboard/DashboardIndex/');
	}
	
}
