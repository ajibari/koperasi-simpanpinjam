<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <?php 
        $plugin = base_url().'assets/plugins/'; 
        $css = base_url().'assets/css/';
        ?>


        <!-- Bootstrap -->
        <link href="<?php echo $plugin; ?>bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo $plugin; ?>font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="<?php echo $css; ?>custom.css" rel="stylesheet">

        <title><?php if(!empty($title)){ echo $title; } else {echo "Backend System";}?></title>
    </head>

    <body class="<?php if(!empty($body_class)){ echo $body_class;}; ?>">

      <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">
          <h2 class="text-center" style="color: #130e0e"><?php echo $this->lang->line('welcome_message');?></h2>

          <div class="animate form login_form">
            <section class="login_content">
              <form method="post" action="<?php echo base_url();?>auth/user/login" id="form-login">
                <h1>Reset Password</h1>
                <div>
                  <input type="text" class="form-control" placeholder="Email" required="" name="email" />
                </div>
                <div>
                  <input type="submit" name="login" value="Reset Password" class="btn btn-default submit" />
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                  <p class="">Copyright 2017</p>
                  <div class="clearfix"></div>
                  <br />

                </div>
              </form>
            </section>
          </div>

          <!-- <div id="register" class="animate form registration_form">
            <section class="login_content">
              <form>
                <h1>Create Account</h1>
                <div>
                  <input type="text" class="form-control" placeholder="Username" required="" />
                </div>
                <div>
                  <input type="email" class="form-control" placeholder="Email" required="" />
                </div>
                <div>
                  <input type="password" class="form-control" placeholder="Password" required="" />
                </div>
                <div>
                  <a class="btn btn-default submit" href="index.html">Submit</a>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                  <p class="change_link">Already a member ?
                    <a href="#signin" class="to_register"> Log in </a>
                  </p>

                  <div class="clearfix"></div>
          
                </div>
              </form>
            </section>
          </div> -->
        </div>
      </div>
      
      <?php 
      $assets = base_url().'assets/plugins/';
      ?>
      <!-- jQuery -->
      <script src="<?php echo $assets; ?>jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap -->
      <script src="<?php echo $assets; ?>bootstrap/js/bootstrap.min.js"></script>

      <script>
      $(document).ready(function(){
        
      });
      </script>

    </body>
</html>