<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends MY_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('master/produk_model','produk');
		$this->load->model('master/satuan_model','satuan');
		$this->load->model('master/mpi_model','mpi');
		$this->load->library('fineuploader');
	}

	public function index(){
		$satuan = $this->satuan->get_all();
		
		$data = array('satuan' => $satuan);
		$this->render('master/produk_index', $data);
	}

	public function json_data(){
		$data = $this->produk->get_all();
		$json['data'] = $data;
		echo json_encode($json);
		exit;
	}

	public function add(){
		
		$qquuid = $this->input->post('qquuid');
		//parameter: ext, size_limit, input_name, chunk_dir, upload_dir
		if(!empty($qquuid)){
			$input = array('upload_dir' => FCPATH.'assets/images/master_produk/');
			$this->fineuploader->upload($input);	
		}
		
		else{
			//data ajax request
			$post = $this->input->post();

			$data = array('id_produk' => $post['kode'], 'nama' => $post['nama'], 'deskripsi' => $post['deskripsi'],'harga_satuan' => $post['harga_satuan'], 'satuan_id' =>$post['satuan'] , 'status' => $post['status']);

			$query = $this->produk->insert($data);

			if($query){
				$response = array('message' => 'sukses');
				echo json_encode($response);
			}

			return false;
		}
	}

	public function edit(){
		//edit produk
		$post = $this->input->post();
		
		$qquuid = $this->input->post('qquuid');

		if(!empty($post['submit']) || !empty($qquuid)){
			//save image	
			if(!empty($qquuid)){
				$input = array('upload_dir' => FCPATH.'assets/images/master_produk/');
				$this->fineuploader->upload($input);

				$img_data = array('id_master_produk' => $post['produk_id'], 'img' => $post['qqfilename'], 'status' => 'y');
				$this->mpi->add($img_data);
				return true;
			}

			//submit edit produk
			$selector = array('id' => $post['id']);
			$data = array('id_produk' => $post['kode'], 'nama' => $post['nama'], 'deskripsi' => $post['deskripsi'],'harga_satuan' => $post['harga_satuan'], 'satuan_id' =>$post['satuan'] , 'status' => $post['status']);

			$update = $this->produk->edit($selector, $data);

			if(!empty($post['img_file'])){
				//del image produk
				$img_file = $post['img_file'];
				$id = $post['img_file'];

				$img_file = $this->mpi->get_images($id);
				$location = FCPATH.'assets/images/master_produk/';
				$response['deleted_id'] = array();

				if($this->general->del_file($location,$img_file)){
					$this->mpi->del_image($id);
					array_push($response['deleted_id'], $id);
				}

			}

			if(isset($response)){
				$response = array_merge($response,array('message' => 'sukses'));
			}
			else{
				$response = array('message' => 'sukses');
			}

			if($update){
				echo json_encode($response);
				exit;
			}

			return false;
		}

		else{
			//request edit produk
			if(!empty($post['id'])){
				$conditions = array('id' => $post['id']);
				$data = $this->produk->get_by($conditions);
				$data['image'] = $this->produk->get_product_images($post['id']);
				echo json_encode($data);
			}

			else{
				redirect(base_url().'dashboard/DashboardIndex');
			}
		}
	}

	public function delete(){
		$post = $this->input->post();

		if($post['submit']){
			$conditions = array('id' => $post['id']);
			if($this->produk->del($conditions)){
				$message = array('message' => 'sukses');
			}

			echo json_encode($message);
		}
		else{
			redirect(base_url().'dashboard/DashboardIndex');
		}
	}

	public function status_change(){
		$post = $this->input->post();
		$status = array('status' => 'y');
		$css_class = 'success';
		$text = 'Aktif';

		if(strtolower($post['value']) === 'y'){
			$status = array('status' => 'n');
			$css_class = 'danger';
			$text = 'Non-Aktif';
		}

		$query = $this->produk->update_status($post['id'], $status);
		
		if($query){
			$response = array('css_class' => $css_class, 'text' => $text, 'status' => $status['status']);	
			echo json_encode($response);
		}
		
		return false;
	}

	public function generate(){
		$this->load->library('htmltopdf');
		$arr = array('html_file' => FCPATH.'application/libraries/res/about.php', 'orientation' => 'P', 'paper_size' => 'A4', 'filename' => 'default', 'download' => 1);
		$this->htmltopdf->generate($arr);
	}
}
