<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends MY_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('master/vendor_model','vendor');
	}

	public function index(){
		$data = array();		
		$this->render('master/vendor_index', $data);
	}

	public function add(){
		//data ajax request
			$post = $this->input->post();

			$data = array('kode' => $post['kode'], 'nama' => $post['nama'], 'deskripsi' => $post['deskripsi'], 'tag' => $post['tag'], 'no_telp' =>$post['no-telp'] , 'email' => $post['email'], 'pic' => $post['pic'], 'alamat' => $post['alamat'], 'status' => $post['status']);

			$query = $this->vendor->insert($data);

			if($query){
				$response = array('message' => 'sukses');
				echo json_encode($response);
			}

			return false;
	}

	public function edit(){
		//edit produk
		$post = $this->input->post();

		if(!empty($post['submit'])){
			//submit edit produk
			$selector = array('id' => $post['id']);
			$data = array('kode' => $post['kode'], 'nama' => $post['nama'], 'level' => $post['level'], 'kategori' => $post['kategori'], 'group' =>$post['group'] , 'status' => $post['status']);

			$update = $this->vendor->update($selector, $data);
			$response = array('message' => 'sukses');

			if($update){
				echo json_encode($response);
				exit;
			}

			return false;
		}

		else{
			//request edit produk
			if(!empty($post['id'])){
				$conditions = array('id' => $post['id']);
				$data = $this->vendor->get_by($conditions);
				echo json_encode($data);
			}

			else{
				redirect(base_url().'dashboard/DashboardIndex');
			}
		}
	}

	public function json_data(){
		$data = $this->vendor->get_all();
		$json['data'] = $data;
		echo json_encode($json);
		exit;
	}

	public function status_change(){
		$post = $this->input->post();
		$status = array('status' => 'y');
		$css_class = 'success';
		$text = 'Aktif';

		if(strtolower($post['value']) === 'y'){
			$status = array('status' => 'n');
			$css_class = 'danger';
			$text = 'Non-Aktif';
		}

		$conditions = array('id' => $post['id']);
		$query = $this->vendor->edit($conditions, $status);
		
		if($query){
			$response = array('css_class' => $css_class, 'text' => $text, 'status' => $status['status']);	
			echo json_encode($response);
		}
		
		return false;
	}

	public function delete(){
		$post = $this->input->post();

		if($post['submit']){
			$conditions = array('id' => $post['id']);
			if($this->vendor->del($conditions)){
				$message = array('message' => 'sukses');
			}

			echo json_encode($message);
		}
		else{
			redirect(base_url().'dashboard/DashboardIndex');
		}
	}
}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */