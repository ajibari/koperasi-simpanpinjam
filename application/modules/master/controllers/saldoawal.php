<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saldoawal extends MY_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('master/saldoawal_model','saldoawal');
		$this->load->model('master/coa_model','coa');
	}

	public function index(){
		$data_coa = $this->coa->autocomplete();
		$data = array('coa' => $data_coa);
		$this->render('master/saldoawal_index', $data);
	}

	public function add(){
		//data ajax request
			$post = $this->input->post();

			$data = array('id_coa' => $post['kode'], 'amount' => $post['amount'], 'status' => $post['status']);

			$query = $this->saldoawal->insert($data);

			if($query){
				$response = array('message' => 'sukses');
				echo json_encode($response);
			}

			return false;
	}

	public function json_data(){
		$data = $this->saldoawal->get_all();
		$json['data'] = $data;
		echo json_encode($json);
		exit;
	}

	public function status_change(){
		$post = $this->input->post();
		$status = array('status' => 'y');
		$css_class = 'success';
		$text = 'Aktif';

		if(strtolower($post['value']) === 'y'){
			$status = array('status' => 'n');
			$css_class = 'danger';
			$text = 'Non-Aktif';
		}

		$conditions = array('id' => $post['id']);
		$query = $this->saldoawal->edit($conditions, $status);
		
		if($query){
			$response = array('css_class' => $css_class, 'text' => $text, 'status' => $status['status']);	
			echo json_encode($response);
		}
		
		return false;
	}

	public function delete(){
		$post = $this->input->post();

		if($post['submit']){
			$conditions = array('id' => $post['id']);
			if($this->saldoawal->del($conditions)){
				$message = array('message' => 'sukses');
			}

			echo json_encode($message);
		}
		else{
			redirect(base_url().'dashboard/DashboardIndex');
		}
	}

}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */