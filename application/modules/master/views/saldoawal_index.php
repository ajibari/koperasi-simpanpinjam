<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <!--<div class="title_left">
                <h3>Plain Page</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Pencarian ...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Cari</button>
                    </span>
                  </div>
                </div>
              </div>-->
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Master Saldo Awal</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <!-- <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li> -->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <!-- Add Modal -->
                    <div id="add-saldoawal-modal" class="modal fade" role="dialog" aria-hidden="true" style="display: none;">
	                    <div class="modal-dialog modal-md">
	                      <div class="modal-content">
							<form id="form-add-saldoawal-master" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		                        <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span>
		                          </button>
		                          <h4 class="modal-title" id="myModalLabel2">Tambah Saldo Awal</h4>
		                        </div>
	                        	
	                        	<div class="modal-body">
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode-coa">Kode <span class="required">*</span>
			                        </label>
			                        <div class="col-md-4 col-sm-6 col-xs-12">
			                          <select id="kode-coa" name="kode" required="required" class="form-control col-md-4 col-xs-6">
			                          		<?php foreach($coa as $k): ?>
												<option value="<?php echo $k['id_coa']; ?>"><?php echo $k['nama']; ?></option>
			                          		<?php endforeach; ?>
			                          </select>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          	<input type="text" id="last-name" name="nama" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="amount" class="control-label col-md-3 col-sm-3 col-xs-12">Amount</label>
			                        <div class="col-md-4 col-sm-6 col-xs-12">
			                          	<input type="text" id="amount" name="amount" required="required" class="form-control col-md-3 col-xs-6">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                          <div id="status" class="btn-group" data-toggle="buttons">
			                            <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="y" data-parsley-multiple="status" checked>Aktif
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="n" data-parsley-multiple="status"> Tidak aktif
			                            </label>
			                          </div>
			                        </div>
			                      </div>
		                        </div>

		                        <div class="modal-footer">
		                          <div class="form-group">
			                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6">
			                          <button type="reset" class="btn btn-primary">Reset</button>
			                          <button type="submit" class="btn btn-success" id="form-add-saldoawal-submit">Simpan</button>
			                        </div>
			                      </div>
		                        </div>
							</form>
	                      </div>
	                    </div>
                  	</div>
					
					<!-- Modal Edit -->
                    <div id="edit-saldoawal-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	                    <div class="modal-dialog modal-md">
	                      <div class="modal-content">
							<form id="form-edit-saldoawal-master" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		                        <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span>
		                          </button>
		                          <h4 class="modal-title" id="myModalLabel2">Edit Saldo Awal</h4>
		                        </div>
	                        	
								<div class="modal-body">
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode-coa">Kode <span class="required">*</span>
			                        </label>
			                        <div class="col-md-4 col-sm-6 col-xs-12">
			                          <input type="text" id="kode-coa" name="kode" required="required" class="form-control col-md-4 col-xs-6">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          	<input type="text" id="last-name" name="nama" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="amount" class="control-label col-md-3 col-sm-3 col-xs-12">Amount</label>
			                        <div class="col-md-4 col-sm-6 col-xs-12">
			                          	<input type="text" id="amount" name="amount" required="required" class="form-control col-md-3 col-xs-6">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                          <div id="status" class="btn-group" data-toggle="buttons">
			                            <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="y" data-parsley-multiple="status" checked>Aktif
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="n" data-parsley-multiple="status"> Tidak aktif
			                            </label>
			                          </div>
			                        </div>
			                      </div>
		                        </div>

		                        <div class="modal-footer">
		                          <div class="form-group">
			                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6">
			                          <button type="reset" class="btn btn-primary">Reset</button>
			                          <button type="submit" class="btn btn-success" id="form-edit-saldoawal-submit">Simpan</button>
			                        </div>
			                      </div>
		                        </div>
							</form>
	                      </div>
	                    </div>
                  	</div>


					<table id="table-saldoawal-master" class="display nowrap" cellspacing="0" width="100%">
						<thead>
				            <tr>
				            	<th></th>
				                <th>Nama</th>
				                <th>Amount</th>
				                <th>Status</th>
				                <th>Aksi</th>
				            </tr>
				        </thead>
				        <tfoot>
				            <tr>
				            	<th></th>
				                <th>Nama</th>
				                <th>Amount</th>
				                <th>Status</th>
				                <th>Aksi</th>
				            </tr>
				        </tfoot>
					</table>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<!-- Fine Uploader Thumbnails template w/ customization
    ====================================================================== -->
    <script type="text/template" id="qq-template-manual-trigger">
        <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Letakkan gambar produk disini">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="buttons">
                <div class="qq-upload-button-selector qq-upload-button">
                    <div>Pilih Gambar</div>
                </div>
                <!-- <button type="button" id="trigger-upload" class="btn btn-primary">
                    <i class="icon-upload icon-white"></i> Upload
                </button> -->
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <div class="qq-progress-bar-container-selector">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Batal</button>
                    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Ulang</button>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Hapus</button>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>

<script>
	$(document).ready(function(){
		var t =  $('#table-saldoawal-master').DataTable({
			dom: 'Bfrtip',
			"columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        },
		    {
	            "targets": -2,
	            "data": "status",
	            "render": function ( data, type, row, meta ) {
	            	var buttonStatus, buttonDel, linkDel;

	            	linkDel = '<?php echo base_url(); ?>master/saldoawal/delete';
	            	linkEdit = '<?php echo base_url(); ?>master/saldoawal/status_change';
	            	buttonDel = '<a class="btn btn-default ajax-del-process" href="'+linkDel+'" data-id="'+row.id+'"><i class="fa fa-trash">&nbsp;</i></a>';

	            	if(data.produk_status == 'y'){
	            		buttonStatus = '<a class="btn btn-success ajax-process" href="<?php echo base_url(); ?>master/saldoawal/status_change" data-id="'+row.id+'" data-value="'+row.status+'" class="btn btn-success">Aktif</a>'+buttonDel;
	            	}else{
	            		buttonStatus = '<a class="btn btn-danger ajax-process" href="<?php echo base_url(); ?>master/saldoawal/status_change" data-id="'+row.id+'" data-value="'+row.status+'" class="btn btn-default">Non-Aktif</a>'+buttonDel;
	            	}

			      return buttonStatus;
			    }
	        },
	        {
	            "targets": -1,
	            "data": "edit",
	            "render": function ( data, type, row, meta ) {
			      return '<a class="btn btn-default edit-modal-produk" data-id="'+data.id+'">Ubah data</a>';
			    }
	        }],
	        "order": [[ 1, 'asc' ]],
			buttons: [
				{ text: 'Tambah Data', action: function(){
					$('#add-saldoawal-modal').modal();
					$('#add-saldoawal-modal').on('shown.bs.modal', function () {
					  $('#kode-produk').focus()
					})
				}},
				'excel', 'pdf',
				{
						extend: 'print',
                        exportOptions: {
	                    columns: [ 0, 1, 2, 3 ]
	                }
				}
			],
			"ajax": "<?php echo base_url()?>master/saldoawal/json_data",
	        "columns": [
	        	{ "data": "id"}, //ini untuk pas di print, ketika di print datatable, dia cetak [object object] kalo di null in, jadi pada request ajax harus ada number dari responnya, ketika di export bener jadinya
	            { "data": "nama" },
	            { "data": "amount" },
	            { "data": null },
	            { "data": null }
	        ]
		});

		t.on( 'order.dt search.dt', function () {
	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

        $('#form-add-saldoawal-master').on('submit', function(e){
        	
        	e.preventDefault();

	        var validateFront = function() {
	          if (true === $('#form-add-saldoawal-master').parsley().isValid()) {
	            $('.bs-callout-info').removeClass('hidden');
	            $('.bs-callout-warning').addClass('hidden');

	            	//check whether there is uploaded file in fineuploader wrapper
				    	if($('#form-add-saldoawal-master').find('ul.qq-upload-list li').length > 0){
				   		    $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
				   		}

				    //submit data produk
				    	$.ajax({
				    		url: '<?php echo base_url(); ?>master/saldoawal/add',
				    		type: 'POST',
				    		dataType: 'json',
				    		data: $('#form-add-saldoawal-master').serialize(),
				    	})
				    	.done(function(response) {
				    		new PNotify({
		                                  title: 'Sukses',
		                                  text: 'Data berhasil disimpan',
		                                  type: 'success',
		                                  styling: 'bootstrap3'
		                              });

					    	$('#fine-uploader-manual-trigger').fineUploader('reset');

				    		$('#form-add-saldoawal-master').trigger("reset");
				    		$('#form-add-saldoawal-master').find('textarea').text('');
				    		t.ajax.reload();

				    	})
				    	.fail(function() {
				    		console.log("error");
				    	})
				    	.always(function() {
				    		console.log("complete");
				    	});
	            
	            return true;
	           	
	          } else {

	            $('.bs-callout-info').addClass('hidden');
	            $('.bs-callout-warning').removeClass('hidden');

	            return false;
	          }
	        };

	        validateFront();
        });

        /**Ajax Process */
		$('#table-saldoawal-master tbody').on( 'click', 'a.ajax-process', function (e) {
			var el = $(this);
		    e.preventDefault();

		    $.ajax({
		        url: $(this).attr('href'),
		        type: 'POST',
		        dataType: 'json',
		        data: {
		            id: $(this).data('id'), 
		            value: $(this).data('value'),
		            submit: 1
		        }
		    })
		    .done(function(response) {
		        new PNotify({
		              title: 'Sukses',
		              text: 'Data berhasil disimpan',
		              type: 'success',
		              styling: 'bootstrap3',
		              delay: 2000
		          });
		        el.removeClass('btn-danger').removeClass('btn-success').addClass('btn-'+response.css_class).text(response.text).data('value', response.status);
		    })
		    .fail(function() {
		        new PNotify({
		              title: 'Gagal',
		              text: 'Data gagal disimpan',
		              type: 'error',
		              styling: 'bootstrap3'
		          });
		    })
		     
		});

		$('#table-saldoawal-master tbody').on( 'click', 'a.ajax-del-process', function (e) {
			var el = $(this);
		    e.preventDefault();

		    $.ajax({
		    	url: $(this).attr('href'),
		    	type: 'POST',
		    	dataType: 'json',
		    	data: {	
		    			id: $(this).data('id'), 
		    			submit: 1
		    		},
		    })
		    .done(function() {
	    		t.ajax.reload();
		    })
		    .fail(function() {
		    	console.log("error");
		    })
		    .always(function() {
		    	console.log("complete");
		    });
		    
		});

		/** Edit Modal */
		$('#table-saldoawal-master tbody').on( 'click', 'a.edit-modal-produk', function (e) {
			var el = $(this);
		    e.preventDefault();

		    $.ajax({
		    	url: '<?php echo base_url(); ?>master/saldoawal/edit',
		    	type: 'POST',
		    	dataType: 'json',
		    	data: {id: $(this).data('id')},
		    })
		    .done(function(response) {
		    	$('#edit-saldoawal-modal').modal();
				$('#edit-saldoawal-modal').on('shown.bs.modal', function () {
				  $('#edit-kode-produk').focus()

				  	/**Generate value */
				  		var radio = $('#edit-saldoawal-modal').find('.radio-status');
				  		$('#edit-saldoawal-modal').find('#edit-kode-produk').val(response.id_produk);
				  		$('#edit-saldoawal-modal').find('#edit-nama').val(response.nama);
				  		$('#edit-saldoawal-modal').find('#edit-deskripsi').text(response.deskripsi);
				  		$('#edit-saldoawal-modal').find('#edit-harga-satuan').val(response.harga_satuan);
				  		$('#edit-saldoawal-modal').find('#id-produk').val(response.id);

				  		radio.parent().removeClass('active');
				  		radio.removeAttr('checked');
				  		$('#edit-saldoawal-modal').find('.radio-status[value="'+response.status+'"]').attr('checked', true);
				  		$('#edit-saldoawal-modal').find('.radio-status[value="'+response.status+'"]').parent().addClass('active');
				})
		    })
		    .fail(function() {
		    	console.log("error");
		    })
		    .always(function() {
		    	console.log("complete");
		    });
		    
		    
		});

		$('#form-edit-saldoawal-master').on('submit', function(e){
        	
        	e.preventDefault();

	        var validateFront = function() {
	          if (true === $('#form-edit-saldoawal-master').parsley().isValid()) {
	            $('.bs-callout-info').removeClass('hidden');
	            $('.bs-callout-warning').addClass('hidden');

	            	//check whether there is uploaded file in fineuploader wrapper
				    	if($('#form-edit-saldoawal-master').find('ul.qq-upload-list li').length > 0){
				   		    $('#fine-uploader-manual-trigger-edit').fineUploader('uploadStoredFiles');
				   		}

				    //submit data produk
				    	$.ajax({
				    		url: '<?php echo base_url(); ?>master/saldoawal/edit',
				    		type: 'POST',
				    		dataType: 'json',
				    		data: $('#form-edit-saldoawal-master').serialize(),
				    	})
				    	.done(function(response) {
				    		new PNotify({
		                                  title: 'Sukses',
		                                  text: 'Data berhasil diubah',
		                                  type: 'success',
		                                  styling: 'bootstrap3'
		                              });

				    		t.ajax.reload();

				    	})
				    	.fail(function() {
				    		console.log("error");
				    	})
				    	.always(function() {
				    		console.log("complete");
				    	});
	            
	            return true;
	           	
	          } else {

	            $('.bs-callout-info').addClass('hidden');
	            $('.bs-callout-warning').removeClass('hidden');

	            return false;
	          }
	        };

	        validateFront();
        });

		$('#kode-coa').select2({
          placeholder: "Pilih COA",
          allowClear: true
        });
        //sidebar select active current page

	});
	
		
</script>

