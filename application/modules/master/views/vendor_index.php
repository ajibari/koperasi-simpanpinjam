<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <!--<div class="title_left">
                <h3>Plain Page</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Pencarian ...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Cari</button>
                    </span>
                  </div>
                </div>
              </div>-->
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Master Vendor</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <!-- <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li> -->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <!-- Modal -->
                    <div id="add-data-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	                    <div class="modal-dialog modal-md">
	                      <div class="modal-content">
							<form id="form-add-vendor-master" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		                        <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                          </button>
		                          <h4 class="modal-title" id="myModalLabel2">Tambah Vendor</h4>
		                        </div>
	                        	
	                        	<div class="modal-body">
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode-produk">Kode Vendor <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="kode-vendor" name="kode" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Vendor <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="nama-vendor" name="nama" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="deskripsi" class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          	<textarea name="deskripsi" class="form-control col-md-7 col-xs-12"></textarea>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="tag" class="control-label col-md-3 col-sm-3 col-xs-12">Tag Vendor</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input id="tag" class="form-control col-md-7 col-xs-12" type="text" name="tag">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="no-telp" class="control-label col-md-3 col-sm-3 col-xs-12">No. Telepon</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input id="no-telp" class="form-control col-md-7 col-xs-12 nomer" type="text" name="no-telp">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input id="email" class="form-control col-md-7 col-xs-12" type="text" name="email">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="pic" class="control-label col-md-3 col-sm-3 col-xs-12">Person in charge (PIC)</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input id="pic" class="form-control col-md-7 col-xs-12" type="text" name="pic">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="satuan" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          	<textarea name="alamat" class="form-control col-md-7 col-xs-12"></textarea>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                          <div id="status" class="btn-group" data-toggle="buttons">
			                            <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="y" data-parsley-multiple="status" checked>Aktif
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="n" data-parsley-multiple="status"> Tidak aktif
			                            </label>
			                          </div>
			                        </div>
			                      </div>
		                        </div>

		                        <div class="modal-footer">
		                          <div class="form-group">
			                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6">
			                          <button type="reset" class="btn btn-primary">Reset</button>
			                          <button type="submit" class="btn btn-success" id="form-add-data-submit">Simpan</button>
			                        </div>
			                      </div>
		                        </div>
							</form>
	                      </div>
	                    </div>
                  	</div>
					
					<!-- Modal Edit -->
                    <div id="edit-vendor-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	                    <div class="modal-dialog modal-md">
	                      <div class="modal-content">
							<form id="form-edit-vendor-master" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		                        <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                          </button>
		                          <h4 class="modal-title" id="myModalLabel2">Edit Vendor</h4>
		                        </div>
	                        	
	                        	<div class="modal-body">
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode-produk">Kode Vendor <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="kode-vendor" name="kode" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Vendor <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="nama-vendor" name="nama" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="deskripsi" class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          	<textarea name="deskripsi" class="form-control col-md-7 col-xs-12"></textarea>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="harga-satuan" class="control-label col-md-3 col-sm-3 col-xs-12">Tag Vendor</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input id="tag" class="form-control col-md-7 col-xs-12" type="text" name="tag">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="no-telp" class="control-label col-md-3 col-sm-3 col-xs-12">No. Telepon</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input id="no-telp" class="form-control col-md-7 col-xs-12 nomer" type="text" name="no-telp">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input id="email" class="form-control col-md-7 col-xs-12" type="text" name="email">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="pic" class="control-label col-md-3 col-sm-3 col-xs-12">Person in charge (PIC)</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input id="pic" class="form-control col-md-7 col-xs-12" type="text" name="pic">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="satuan" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          	<textarea name="alamat" class="form-control col-md-7 col-xs-12"></textarea>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                          <div id="status" class="btn-group" data-toggle="buttons">
			                            <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="y" data-parsley-multiple="status" checked>Aktif
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="n" data-parsley-multiple="status"> Tidak aktif
			                            </label>
			                          </div>
			                        </div>
			                      </div>
		                        </div>

		                        <div class="modal-footer">
		                          <div class="form-group">
			                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6">
			                          <button type="reset" class="btn btn-primary">Reset</button>
			                          <button type="submit" class="btn btn-success" id="form-add-data-submit">Simpan</button>
			                        </div>
			                      </div>
		                        </div>
							</form>
	                      </div>
	                    </div>
                  	</div>

					<div class="datatable-div-wrapper">
						<table id="table-vendor-master" class="display nowrap" cellspacing="0" width="100%">
						<thead>
				            <tr>
				            	<th></th>
				                <th>Nama</th>
				                <th>Deskripsi</th>
				                <th>Tag</th>
				                <th>PIC</th>
				                <th>Alamat</th>
				                <th>No Telp</th>
				                <th>Email</th>
				                <th>Status</th>
				                <th>Aksi</th>
				            </tr>
				        </thead>
				        <tfoot>
				            <tr>
				            	<th></th>
				                <th>Nama</th>
				                <th>Deskripsi</th>
				                <th>Tag</th>
				                <th>PIC</th>
				                <th>Alamat</th>
				                <th>No Telp</th>
				                <th>Email</th>
				                <th>Status</th>
				                <th>Aksi</th>
				            </tr>
				        </tfoot>
						</table>
					</div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

<script>
	$(document).ready(function(){
		var t =  $('#table-vendor-master').DataTable({
			dom: 'Bfrtip',
			"columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        },
		    {
	            "targets": -2,
	            "data": "status",
	            "render": function ( data, type, row, meta ) {
	            	var buttonStatus, buttonDel, linkDel;

	            	linkDel = '<?php echo base_url(); ?>master/vendor/delete';
	            	linkEdit = '<?php echo base_url(); ?>master/vendor/status_change';
	            	buttonDel = '<a class="btn btn-default ajax-del-process" href="'+linkDel+'" data-id="'+row.id+'"><i class="fa fa-trash">&nbsp;</i></a>';

	            	if(data.status == 'y'){
	            		buttonStatus = '<a class="btn btn-success ajax-process" href="'+linkEdit+'" data-id="'+row.id+'" data-value="'+row.status+'" class="btn btn-success">Aktif</a>'+buttonDel;
	            	}else{
	            		buttonStatus = '<a class="btn btn-danger ajax-process" href="'+linkEdit+'" data-id="'+row.id+'" data-value="'+row.status+'" class="btn btn-default">Non-Aktif</a>'+buttonDel;
	            	}

			      	return buttonStatus;
			    }
	        },
	        {
	            "targets": -1,
	            "data": "edit",
	            "render": function ( data, type, row, meta ) {
			      return '<a class="btn btn-default edit-modal-vendor" data-id="'+data.id+'">Ubah data</a>';
			    }
	        }],
	        "order": [[ 1, 'asc' ]],
			buttons: [
				{ text: 'Tambah Data', action: function(){
					$('#add-data-modal').modal();
					$('#add-data-modal').on('shown.bs.modal', function () {
					  $('#kode-produk').focus()
					})
				}},
				'excel', 'pdf',
				{
						extend: 'print',
                        exportOptions: {
	                    columns: [ 0, 1, 2, 3 ]
	                }
				}
			],
			"ajax": "<?php echo base_url()?>master/vendor/json_data",
	        "columns": [
	        	{ "data": "id"}, //ini untuk pas di print, ketika di print datatable, dia cetak [object object] kalo di null in, jadi pada request ajax harus ada number dari responnya, ketika di export bener jadinya
	            { "data": "nama" },
	            { "data": "deskripsi" },
	            { "data": "tag" },
	            { "data": "pic" },
	            { "data": "alamat" },
	            { "data": "no_telp" },
	            { "data": "email" },
	            { "data": null },
	            { "data": null },
	        ]
		});

		t.on( 'order.dt search.dt', function () {
	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

		$('#tag').tagsInput({
          width: 'auto'
        });

		$('.nomer').jStepper({minValue:0, maxValue:999999999999, minLength:10});

        $('#form-add-vendor-master').on('submit', function(e){
        	
        	e.preventDefault();

	        var validateFront = function() {
	          if (true === $('#form-add-vendor-master').parsley().isValid()) {
	            $('.bs-callout-info').removeClass('hidden');
	            $('.bs-callout-warning').addClass('hidden');

				    //submit data produk
				    	$.ajax({
				    		url: '<?php echo base_url(); ?>master/vendor/add',
				    		type: 'POST',
				    		dataType: 'json',
				    		data: $('#form-add-vendor-master').serialize(),
				    	})
				    	.done(function(response) {
				    		new PNotify({
		                                  title: 'Sukses',
		                                  text: 'Data berhasil disimpan',
		                                  type: 'success',
		                                  styling: 'bootstrap3'
		                              });

				    		$('#form-add-vendor-master').trigger("reset");
				    		$('#form-add-vendor-master').find('textarea').text('');
				    		t.ajax.reload();

				    	})
				    	.fail(function() {
				    		console.log("error");
				    	})
				    	.always(function() {
				    		console.log("complete");
				    	});
	            
	            return true;
	           	
	          } else {

	            $('.bs-callout-info').addClass('hidden');
	            $('.bs-callout-warning').removeClass('hidden');

	            return false;
	          }
	        };

	        validateFront();
        });

        /**Ajax Process */
		$('#table-vendor-master tbody').on( 'click', 'a.ajax-process', function (e) {
			var el = $(this);
		    e.preventDefault();

		    $.ajax({
		        url: $(this).attr('href'),
		        type: 'POST',
		        dataType: 'json',
		        data: {
		            id: $(this).data('id'), 
		            value: $(this).data('value')
		        }
		    })
		    .done(function(response) {
		        new PNotify({
		              title: 'Sukses',
		              text: 'Data berhasil disimpan',
		              type: 'success',
		              styling: 'bootstrap3',
		              delay: 2000
		          });
		        el.removeClass('btn-danger').removeClass('btn-success').addClass('btn-'+response.css_class).text(response.text).data('value', response.status);
		    })
		    .fail(function() {
		        new PNotify({
		              title: 'Gagal',
		              text: 'Data gagal disimpan',
		              type: 'error',
		              styling: 'bootstrap3'
		          });
		    })
		    		    
		});

		$('#table-vendor-master tbody').on( 'click', 'a.ajax-del-process', function (e) {
			var el = $(this);
		    e.preventDefault();

		    $.ajax({
		    	url: $(this).attr('href'),
		    	type: 'POST',
		    	dataType: 'json',
		    	data: {	
		    			id: $(this).data('id'), 
		    			submit: 1
		    		},
		    })
		    .done(function() {
	    		t.ajax.reload();
		    })
		    .fail(function() {
		    	console.log("error");
		    })
		    .always(function() {
		    	console.log("complete");
		    });
		    
		});

		/** Edit Modal */
		$('#table-vendor-master tbody').on( 'click', 'a.edit-modal-vendor', function (e) {
			var el = $(this);
		    e.preventDefault();

		    $.ajax({
		    	url: '<?php echo base_url(); ?>master/vendor/edit',
		    	type: 'POST',
		    	dataType: 'json',
		    	data: {id: $(this).data('id')},
		    })
		    .done(function(response) {
		    	$('#edit-vendor-modal').modal();
				$('#edit-vendor-modal').on('shown.bs.modal', function () {
				  $('#edit-kode-produk').focus()

				  	/**Generate value */
				  		var radio = $('#edit-vendor-modal').find('.radio-status');
				  		$('#edit-vendor-modal').find('#edit-kode-produk').val(response.id_produk);
				  		$('#edit-vendor-modal').find('#edit-nama').val(response.nama);
				  		$('#edit-vendor-modal').find('#edit-deskripsi').text(response.deskripsi);
				  		$('#edit-vendor-modal').find('#edit-harga-satuan').val(response.harga_satuan);
				  		$('#edit-vendor-modal').find('#id-produk').val(response.id);

				  		radio.parent().removeClass('active');
				  		radio.removeAttr('checked');
				  		$('#edit-vendor-modal').find('.radio-status[value="'+response.status+'"]').attr('checked', true);
				  		$('#edit-vendor-modal').find('.radio-status[value="'+response.status+'"]').parent().addClass('active');
				})
		    })
		    .fail(function() {
		    	console.log("error");
		    })
		    .always(function() {
		    	console.log("complete");
		    });
		    	    
		});

		$('[type="reset"]').on('click', function(){
			$('textarea').text('');
		});


		$('#form-edit-vendor-master').on('submit', function(e){
        	
        	e.preventDefault();

	        var validateFront = function() {
	          if (true === $('#form-edit-vendor-master').parsley().isValid()) {
	            $('.bs-callout-info').removeClass('hidden');
	            $('.bs-callout-warning').addClass('hidden');

				    //submit data produk
				    	$.ajax({
				    		url: '<?php echo base_url(); ?>master/vendor/edit',
				    		type: 'POST',
				    		dataType: 'json',
				    		data: $('#form-edit-vendor-master').serialize(),
				    	})
				    	.done(function(response) {
				    		new PNotify({
		                                  title: 'Sukses',
		                                  text: 'Data berhasil diubah',
		                                  type: 'success',
		                                  styling: 'bootstrap3'
		                              });

				    		t.ajax.reload();

				    	})
				    	.fail(function() {
				    		console.log("error");
				    	})
				    	.always(function() {
				    		console.log("complete");
				    	});
	            
	            return true;
	           	
	          } else {

	            $('.bs-callout-info').addClass('hidden');
	            $('.bs-callout-warning').removeClass('hidden');

	            return false;
	          }
	        };

	        validateFront();
        });

        //sidebar select active current page

	});
	
		
</script>

