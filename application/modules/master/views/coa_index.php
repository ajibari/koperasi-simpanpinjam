<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <!--<div class="title_left">
                <h3>Plain Page</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Pencarian ...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Cari</button>
                    </span>
                  </div>
                </div>
              </div>-->
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Master COA</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <!-- <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li> -->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <!-- Add Modal -->
                    <div id="add-coa-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	                    <div class="modal-dialog modal-md">
	                      <div class="modal-content">
							<form id="form-add-coa-master" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		                        <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span>
		                          </button>
		                          <h4 class="modal-title" id="myModalLabel2">Tambah Chart of Account</h4>
		                        </div>
	                        	
	                        	<div class="modal-body">
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode-coa">Kode <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="kode-coa" name="kode" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="last-name" name="nama" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="deskripsi" class="control-label col-md-3 col-sm-3 col-xs-12">Level</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <select name="level" class="form-control col-md-7 col-xs-12">
			                          	<?php for ($i=1; $i <= 10; $i++): ?>
			                          		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
			                          	<?php endfor; ?>
			                          </select>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="kategori" class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                          <div id="kategori" class="btn-group" data-toggle="buttons">
			                            <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="kategori" value="cash" data-parsley-multiple="status" checked>Cash
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="kategori" value="bank" data-parsley-multiple="status">Bank
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="kategori" value="other" data-parsley-multiple="status">Other
			                            </label>
			                          </div>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="group" class="control-label col-md-3 col-sm-3 col-xs-12">Grup</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <select name="group" class="form-control col-md-7 col-xs-12">
			                          		<?php if(!empty($group)){
			                          			foreach($group as $g): 
			                          		?>
			                          				<option value="<?php echo $g['id']; ?>"><?php echo $g['nama']; ?></option>
			                          		<?php 
			                          			endforeach;
			                          			}
			                          		?>
			                          </select>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                          <div id="status" class="btn-group" data-toggle="buttons">
			                            <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="y" data-parsley-multiple="status" checked>Aktif
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="n" data-parsley-multiple="status"> Tidak aktif
			                            </label>
			                          </div>
			                        </div>
			                      </div>
		                        </div>

		                        <div class="modal-footer">
		                          <div class="form-group">
			                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6">
			                          <button type="reset" class="btn btn-primary">Reset</button>
			                          <button type="submit" class="btn btn-success" id="form-add-coa-submit">Simpan</button>
			                        </div>
			                      </div>
		                        </div>
							</form>
	                      </div>
	                    </div>
                  	</div>
					
					<!-- Modal Edit -->
                    <div id="edit-coa-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	                    <div class="modal-dialog modal-md">
	                      <div class="modal-content">
							<form id="form-edit-coa-master" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		                        <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span>
		                          </button>
		                          <h4 class="modal-title" id="myModalLabel2">Edit Chart of Account</h4>
		                        </div>
	                        	
	                        	<div class="modal-body">
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode-coa">Kode <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="edit-kode-coa" name="kode" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="edit-nama" name="nama" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="deskripsi" class="control-label col-md-3 col-sm-3 col-xs-12">Level</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <select name="level" class="form-control col-md-7 col-xs-12" id="edit-level">
			                          	<?php for ($i=1; $i <= 10; $i++): ?>
			                          		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
			                          	<?php endfor; ?>
			                          </select>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="harga-satuan" class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                          <div id="kategori" class="btn-group" data-toggle="buttons">
			                            <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="kategori" value="cash" checked>Cash
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="kategori" value="bank">Bank
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="kategori" value="other">Other
			                            </label>
			                          </div>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="group" class="control-label col-md-3 col-sm-3 col-xs-12">Grup</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <select name="group" class="form-control col-md-7 col-xs-12">
			                          		<?php if(!empty($group)){
			                          			foreach($group as $g): 
			                          		?>
			                          				<option value="<?php echo $g['id']; ?>"><?php echo $g['nama']; ?></option>
			                          		<?php 
			                          			endforeach;
			                          			}
			                          		?>
			                          </select>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                          <div id="status" class="btn-group" data-toggle="buttons">
			                            <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="y" data-parsley-multiple="status" checked>Aktif
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="n" data-parsley-multiple="status"> Tidak aktif
			                            </label>
			                          </div>
			                        </div>
			                      </div>
		                        </div>

		                        <div class="modal-footer">
		                          <div class="form-group">
			                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6">
			                          <input type="hidden" name="submit" value="1">
			                          <input type="hidden" name="id">

			                          <button type="reset" class="btn btn-primary">Reset</button>
			                          <button type="submit" class="btn btn-success" id="form-edit-coa-submit">Simpan</button>
			                        </div>
			                      </div>
		                        </div>
							</form>
	                      </div>
	                    </div>
                  	</div>

					<div class="datatable-div-wrapper">
						<table id="table-coa-master" class="display nowrap" cellspacing="0" width="100%">
						<thead>
				            <tr>
				            	<th></th>
				                <th>ID COA</th>
				                <th>Nama</th>
				                <th>Kategori</th>
				                <th>Status</th>
				                <th>Aksi</th>
				            </tr>
				        </thead>
				        <tfoot>
				            <tr>
				            	<th></th>
				                <th>ID COA</th>
				                <th>Nama</th>
				                <th>Kategori</th>
				                <th>Status</th>
				                <th>Aksi</th>
				            </tr>
				        </tfoot>
						</table>
					</div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

<script>
	$(document).ready(function(){
		var t =  $('#table-coa-master').DataTable({
			dom: 'Bfrtip',
			"columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        },
		    {
	            "targets": -2,
	            "data": "status",
	            "render": function ( data, type, row, meta ) {
			      	var buttonStatus, buttonDel, linkDel;
	            	
	            	linkDel = '<?php echo base_url(); ?>master/coa/delete';
	            	linkEdit = '<?php echo base_url(); ?>master/coa/status_change';
	            	buttonDel = '<a class="btn btn-default ajax-del-process" href="'+linkDel+'" data-id="'+row.id+'"><i class="fa fa-trash">&nbsp;</i></a>';

	            	if(data.status == 'y'){
	            		buttonStatus = '<a class="btn btn-success ajax-process" href="'+linkEdit+'" data-id="'+row.id+'" data-value="'+row.status+'" class="btn btn-success">Aktif</a>'+buttonDel;
	            	}else{
	            		buttonStatus = '<a class="btn btn-danger ajax-process" href="'+linkEdit+'" data-id="'+row.id+'" data-value="'+row.status+'" class="btn btn-default">Non-Aktif</a>'+buttonDel;
	            	}

			      return buttonStatus;



			      	
			      	

	            	if(data.produk_status == 'y'){
	            		buttonStatus = '<a class="btn btn-success ajax-process" href="'+linkEdit+'" data-id="'+row.id+'" data-value="'+row.status+'" class="btn btn-success">Aktif</a>'+buttonDel;
	            	}else{
	            		buttonStatus = '<a class="btn btn-danger ajax-process" href="'+linkEdit+'" data-id="'+row.id+'" data-value="'+row.status+'" class="btn btn-default">Non-Aktif</a>'+buttonDel;
	            	}

			      return buttonStatus;

			    }
	        },
	        {
	            "targets": -1,
	            "data": "edit",
	            "render": function ( data, type, row, meta ) {
			      return '<a class="btn btn-default edit-modal-produk" data-id="'+data.id+'">Ubah data</a>';
			    }
	        }],
	        "order": [[ 1, 'asc' ]],
			buttons: [
				{ text: 'Tambah Data', action: function(){
					$('#add-coa-modal').modal();
					$('#add-coa-modal').on('shown.bs.modal', function () {
					  $('#kode-produk').focus()
					})
				}},
				'excel', 'pdf',
				{
						extend: 'print',
                        exportOptions: {
	                    columns: [ 0, 1, 2, 3 ]
	                }
				}
			],
			"ajax": "<?php echo base_url()?>master/coa/json_data",
	        "columns": [
	        	{ "data": "id"}, //ini untuk pas di print, ketika di print datatable, dia cetak [object object] kalo di null in, jadi pada request ajax harus ada number dari responnya, ketika di export bener jadinya
	            { "data": "kode" },
	            { "data": "nama" },
	            { "data": "kategori" },
	            { "data": null },
	            { "data": null }
	        ]
		});

		t.on( 'order.dt search.dt', function () {
	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

        $('#form-add-coa-master').on('submit', function(e){
        	
        	e.preventDefault();

	        var validateFront = function() {
	          if (true === $('#form-add-coa-master').parsley().isValid()) {
	            $('.bs-callout-info').removeClass('hidden');
	            $('.bs-callout-warning').addClass('hidden');

	            	//check whether there is uploaded file in fineuploader wrapper
				    	if($('#form-add-coa-master').find('ul.qq-upload-list li').length > 0){
				   		    $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
				   		}

				    //submit data produk
				    	$.ajax({
				    		url: '<?php echo base_url(); ?>master/coa/add',
				    		type: 'POST',
				    		dataType: 'json',
				    		data: $('#form-add-coa-master').serialize(),
				    	})
				    	.done(function(response) {
				    		new PNotify({
		                                  title: 'Sukses',
		                                  text: 'Data berhasil disimpan',
		                                  type: 'success',
		                                  styling: 'bootstrap3'
		                              });


				    		$('#form-add-coa-master').trigger("reset");
				    		t.ajax.reload();

				    	})
				    	.fail(function() {
				    		console.log("error");
				    	})
				    	.always(function() {
				    		console.log("complete");
				    	});
	            
	            return true;
	           	
	          } else {

	            $('.bs-callout-info').addClass('hidden');
	            $('.bs-callout-warning').removeClass('hidden');

	            return false;
	          }
	        };

	        validateFront();
        });

        /**Ajax Process */
		$('#table-coa-master tbody').on( 'click', 'a.ajax-process', function (e) {
			var el = $(this);
		    e.preventDefault();

		    $.ajax({
		        url: $(this).attr('href'),
		        type: 'POST',
		        dataType: 'json',
		        data: {
		            id: $(this).data('id'), 
		            value: $(this).data('value')
		        }
		    })
		    .done(function(response) {
		        new PNotify({
		              title: 'Sukses',
		              text: 'Data berhasil disimpan',
		              type: 'success',
		              styling: 'bootstrap3',
		              delay: 2000
		          });
		        el.removeClass('btn-danger').removeClass('btn-success').addClass('btn-'+response.css_class).text(response.text).data('value', response.status);
		    })
		    .fail(function() {
		        new PNotify({
		              title: 'Gagal',
		              text: 'Data gagal disimpan',
		              type: 'error',
		              styling: 'bootstrap3'
		          });
		    })
		    
		    
		});

		$('#table-coa-master tbody').on( 'click', 'a.ajax-del-process', function (e) {
			var el = $(this);
		    e.preventDefault();

		    $.ajax({
		    	url: $(this).attr('href'),
		    	type: 'POST',
		    	dataType: 'json',
		    	data: {	
		    			id: $(this).data('id'), 
		    			submit: 1
		    		},
		    })
		    .done(function() {
	    		t.ajax.reload();
		    })
		    .fail(function() {
		    	console.log("error");
		    })
		    .always(function() {
		    	console.log("complete");
		    });
		    
		});

		/** Edit Modal */
		$('#table-coa-master tbody').on( 'click', 'a.edit-modal-produk', function (e) {
			var el = $(this);
		    e.preventDefault();

		    $.ajax({
		    	url: '<?php echo base_url(); ?>master/coa/edit',
		    	type: 'POST',
		    	dataType: 'json',
		    	data: {id: $(this).data('id')},
		    })
		    .done(function(response) {
		    	$('#edit-coa-modal').modal();
				$('#edit-coa-modal').on('shown.bs.modal', function () {
				  $('#edit-kode-coa').focus()

				  	/**Generate value */
				  		var radio = $('#edit-coa-modal').find('.radio-status');
				  		$('#edit-coa-modal').find('#edit-kode-coa').val(response.kode);
				  		$('#edit-coa-modal').find('#edit-nama').val(response.nama);
				  		$('#edit-coa-modal').find('#edit-level').val(response.level);
				  		$('#edit-coa-modal').find([name='kategori']).val(response.kategori);
				  		$('#edit-coa-modal').find('input[name="id"]').val(response.id);
				  		$('#edit-coa-modal').find('select[name="group"]').val(response.group);

				  		radio.parent().removeClass('active');
				  		radio.removeAttr('checked');
				  		$('#edit-coa-modal').find('.radio-status[value="'+response.status+'"]').attr('checked', true);
				  		$('#edit-coa-modal').find('.radio-status[value="'+response.status+'"]').prop("checked", true);
				  		$('#edit-coa-modal').find('.radio-status[value="'+response.status+'"]').parent().addClass('active');
				})
		    })
		    .fail(function() {
		    	console.log("error");
		    })
		    .always(function() {
		    	console.log("complete");
		    });
		    
		    
		});

		$('[type="reset"]').on('click', function(){
			$('textarea').text('');
		});


		$('#form-edit-coa-master').on('submit', function(e){
        	
        	e.preventDefault();

	        var validateFront = function() {
	          if (true === $('#form-edit-coa-master').parsley().isValid()) {
	            $('.bs-callout-info').removeClass('hidden');
	            $('.bs-callout-warning').addClass('hidden');

				    //submit data produk
				    	$.ajax({
				    		url: '<?php echo base_url(); ?>master/coa/edit',
				    		type: 'POST',
				    		dataType: 'json',
				    		data: $('#form-edit-coa-master').serialize(),
				    	})
				    	.done(function(response) {
				    		new PNotify({
		                                  title: 'Sukses',
		                                  text: 'Data berhasil diubah',
		                                  type: 'success',
		                                  styling: 'bootstrap3'
		                              });

				    		t.ajax.reload();

				    	})
				    	.fail(function() {
				    		console.log("error");
				    	})
				    	.always(function() {
				    		console.log("complete");
				    	});
	            
	            return true;
	           	
	          } else {

	            $('.bs-callout-info').addClass('hidden');
	            $('.bs-callout-warning').removeClass('hidden');

	            return false;
	          }
	        };

	        validateFront();
        });

        //sidebar select active current page

	});
	
		
</script>

