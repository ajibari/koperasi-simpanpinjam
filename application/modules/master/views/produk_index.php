<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <!--<div class="title_left">
                <h3>Plain Page</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Pencarian ...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Cari</button>
                    </span>
                  </div>
                </div>
              </div>-->
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Master Produk</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <!-- <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li> -->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <!-- Modal -->
                    <div id="add-data-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	                    <div class="modal-dialog modal-md">
	                      <div class="modal-content">
							<form id="form-add-data-master" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		                        <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                          </button>
		                          <h4 class="modal-title" id="myModalLabel2">Tambah Produk</h4>
		                        </div>
	                        	
	                        	<div class="modal-body">
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode-produk">Kode Produk <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="kode-produk" name="kode" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Produk <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="last-name" name="nama" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="deskripsi" class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <textarea name="deskripsi" class="form-control col-md-7 col-xs-12"></textarea>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="harga-satuan" class="control-label col-md-3 col-sm-3 col-xs-12">Harga satuan</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input id="harga-satuan" class="form-control col-md-7 col-xs-12" type="text" name="harga_satuan">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="satuan" class="control-label col-md-3 col-sm-3 col-xs-12">Satuan</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <select name="satuan" class="form-control col-md-7 col-xs-12">
			                          		<?php if(!empty($satuan)){
			                          			foreach($satuan as $s): 
			                          		?>
			                          				<option value="<?php echo $s['id']; ?>"><?php echo $s['nama']; ?></option>
			                          		<?php 
			                          			endforeach;
			                          			}
			                          		?>
			                          </select>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                          <div id="status" class="btn-group" data-toggle="buttons">
			                            <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="y" checked>Aktif
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" name="status" value="n"> Tidak aktif
			                            </label>
			                          </div>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                      	<label for="image" class="control-label col-md-3 col-sm-3 col-xs-12">Gambar Produk</label>
			                      	<div class="col-md-7 col-sm-12 col-xs-12">
			                      		<div id="fine-uploader-manual-trigger"></div>
			                      	</div>
			                      </div>
		                        </div>

		                        <div class="modal-footer">
		                          <div class="form-group">
			                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6">
			                          <button type="reset" class="btn btn-primary">Reset</button>
			                          <button type="submit" class="btn btn-success" id="form-add-data-submit">Simpan</button>
			                        </div>
			                      </div>
		                        </div>
							</form>
	                      </div>
	                    </div>
                  	</div>
					
					<!-- Modal Edit -->
                    <div id="edit-data-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	                    <div class="modal-dialog modal-md">
	                      <div class="modal-content">
							<form id="form-edit-data-master" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		                        <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                          </button>
		                          <h4 class="modal-title" id="myModalLabel2">Edit Produk</h4>
		                        </div>
	                        	
	                        	<div class="modal-body">
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode-produk">Kode Produk <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="edit-kode-produk" name="kode" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Produk <span class="required">*</span>
			                        </label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input type="text" id="edit-nama" name="nama" required="required" class="form-control col-md-7 col-xs-12">
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="deskripsi" class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <textarea id="edit-deskripsi" name="deskripsi" class="form-control col-md-7 col-xs-12"></textarea>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="harga-satuan" class="control-label col-md-3 col-sm-3 col-xs-12">Harga satuan</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <input id="edit-harga-satuan" class="form-control col-md-7 col-xs-12" type="text" name="harga_satuan">
			                          <input type="hidden" name="submit" value="1" />
			                          <input type="hidden" name="id" id="id-produk" />
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label for="satuan" class="control-label col-md-3 col-sm-3 col-xs-12">Satuan</label>
			                        <div class="col-md-7 col-sm-6 col-xs-12">
			                          <select name="satuan" id="edit-satuan" class="form-control col-md-7 col-xs-12">
			                          		<?php if(!empty($satuan)){
			                          			foreach($satuan as $s): 
			                          		?>
			                          				<option value="<?php echo $s['id']; ?>"><?php echo $s['nama']; ?></option>
			                          		<?php 
			                          			endforeach;
			                          			}
			                          		?>
			                          </select>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
			                        <div class="col-md-6 col-sm-6 col-xs-12">
			                          <div id="edit-status" class="btn-group" data-toggle="buttons">
			                            <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" class="radio-status" name="status" value="y" data-parsley-multiple="status">Aktif
			                            </label>
			                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
			                              <input type="radio" class="radio-status" name="status" value="n" data-parsley-multiple="status">Tidak aktif
			                            </label>
			                          </div>
			                        </div>
			                      </div>
			                      <div class="form-group">
			                      	<label for="image" class="control-label col-md-3 col-sm-3 col-xs-12">Gambar Produk</label>
			                      	<div class="col-md-7 col-sm-12 col-xs-12" id="image_wrapper">
			                      		<div id="fine-uploader-manual-trigger-edit"></div>
			                      	</div>
			                      </div>
		                        </div>

		                        <div class="modal-footer">
		                          <div class="form-group">
			                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6">
			                          <button type="reset" class="btn btn-primary">Reset</button>
			                          <button type="submit" class="btn btn-success" id="form-add-data-submit">Simpan</button>
			                        </div>
			                      </div>
		                        </div>
							</form>
	                      </div>
	                    </div>
                  	</div>

					<div class="datatable-div-wrapper">
						<table id="table-produk-master" class="display nowrap" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th></th>
					                <th>ID Produk</th>
					                <th>Nama</th>
					                <th>Harga Satuan</th>
					                <th>Satuan</th>
					                <th>Status</th>
					                <th>Aksi</th>
					            </tr>
					        </thead>
					        <tfoot>
					            <tr>
					            	<th></th>
					                <th>ID Produk</th>
					                <th>Nama</th>
					                <th>Harga Satuan</th>
					                <th>Satuan</th>
					                <th>Status</th>
					                <th>Aksi</th>
					            </tr>
					        </tfoot>
						</table>
					</div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<!-- Fine Uploader Thumbnails template w/ customization
    ====================================================================== -->
    <script type="text/template" id="qq-template-manual-trigger">
        <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Letakkan gambar produk disini">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="buttons">
                <div class="qq-upload-button-selector qq-upload-button">
                    <div>Pilih Gambar</div>
                </div>
                <!-- <button type="button" id="trigger-upload" class="btn btn-primary">
                    <i class="icon-upload icon-white"></i> Upload
                </button> -->
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <div class="qq-progress-bar-container-selector">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Batal</button>
                    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Ulang</button>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Hapus</button>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>

<script>
	$(document).ready(function(){
		var t =  $('#table-produk-master').DataTable({
			dom: 'Bfrtip',
			"columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        },
		    {
	            "targets": -2,
	            "data": "status",
	            "render": function ( data, type, row, meta ) {
			      	var buttonStatus, buttonDel, linkDel;
			      	
			      	linkDel = '<?php echo base_url(); ?>master/produk/delete';
	            	linkEdit = '<?php echo base_url(); ?>master/produk/status_change';
	            	buttonDel = '<a class="btn btn-default ajax-del-process" href="'+linkDel+'" data-id="'+row.id+'"><i class="fa fa-trash">&nbsp;</i></a>';

	            	if(data.produk_status == 'y'){
	            		buttonStatus = '<a class="btn btn-success ajax-process" href="'+linkEdit+'" data-id="'+row.id+'" data-value="'+row.status+'" class="btn btn-success">Aktif</a>'+buttonDel;
	            	}else{
	            		buttonStatus = '<a class="btn btn-danger ajax-process" href="'+linkEdit+'" data-id="'+row.id+'" data-value="'+row.status+'" class="btn btn-default">Non-Aktif</a>'+buttonDel;
	            	}

			      	return buttonStatus;
			    }
	        },
	        {
	            "targets": -1,
	            "data": "edit",
	            "render": function ( data, type, row, meta ) {
			      return '<a class="btn btn-default edit-modal-produk" data-id="'+data.id+'">Ubah data</a>';
			    }
	        }],
	        "order": [[ 1, 'asc' ]],
			buttons: [
				{ text: 'Tambah Data', action: function(){
					$('#add-data-modal').modal();
					$('#add-data-modal').on('shown.bs.modal', function () {
					  $('#kode-produk').focus()
					})
				}},
				'excel', 'pdf',
				{
						extend: 'print',
                        exportOptions: {
	                    columns: [ 0, 1, 2, 3 ]
	                }
				}
			],
			"ajax": "<?php echo base_url()?>master/produk/json_data",
	        "columns": [
	        	{ "data": "id"}, //ini untuk pas di print, ketika di print datatable, dia cetak [object object] kalo di null in, jadi pada request ajax harus ada number dari responnya, ketika di export bener jadinya
	            { "data": "id_produk" },
	            { "data": "nama" },
	            { "data": "harga_satuan" },
	            { "data": "satuan" },
	            { "data": null },
	            { "data": null }
	        ]
		});

		t.on( 'order.dt search.dt', function () {
	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();
		

        $('#fine-uploader-manual-trigger').fineUploader({
            template: 'qq-template-manual-trigger',
            request: {
                endpoint: '<?php echo base_url(); ?>master/produk/add'
            },
            validation: {
            	minSizeLimit: 1000,
            },
            thumbnails: {
                placeholders: {
                    waitingPath: '<?php echo base_url(); ?>assets/images/waiting-generic.png',
                    notAvailablePath: '<?php echo base_url(); ?>assets/images/not_available-generic.png'
                }
            },
            autoUpload: false
        });
        

        $('#form-add-data-master button[type="reset"]').on('click', function(){
		    $('#fine-uploader-manual-trigger').fineUploader('reset');
        });

        $('#form-add-data-master').on('submit', function(e){
        	
        	e.preventDefault();

	        var validateFront = function() {
	          if (true === $('#form-add-data-master').parsley().isValid()) {
	            $('.bs-callout-info').removeClass('hidden');
	            $('.bs-callout-warning').addClass('hidden');

	            	//check whether there is uploaded file in fineuploader wrapper
				    	if($('#form-add-data-master').find('ul.qq-upload-list li').length > 0){
				   		    $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
				   		}

				    //submit data produk
				    	$.ajax({
				    		url: '<?php echo base_url(); ?>master/produk/add',
				    		type: 'POST',
				    		dataType: 'json',
				    		data: $('#form-add-data-master').serialize(),
				    	})
				    	.done(function(response) {
				    		new PNotify({
		                                  title: 'Sukses',
		                                  text: 'Data berhasil disimpan',
		                                  type: 'success',
		                                  styling: 'bootstrap3'
		                              });

					    	$('#fine-uploader-manual-trigger').fineUploader('reset');

				    		$('#form-add-data-master').trigger("reset");
				    		$('#form-add-data-master').find('textarea').text('');
				    		t.ajax.reload();

				    	})
				    	.fail(function() {
				    		console.log("error");
				    	})
				    	.always(function() {
				    		console.log("complete");
				    	});
	            
	            return true;
	           	
	          } else {

	            $('.bs-callout-info').addClass('hidden');
	            $('.bs-callout-warning').removeClass('hidden');

	            return false;
	          }
	        };

	        validateFront();
        });

        /**Ajax Process */
		$('#table-produk-master tbody').on( 'click', 'a.ajax-process', function (e) {
			var el = $(this);
		    e.preventDefault();

		    $.ajax({
		        url: $(this).attr('href'),
		        type: 'POST',
		        dataType: 'json',
		        data: {
		            id: $(this).data('id'), 
		            value: $(this).data('value')
		        }
		    })
		    .done(function(response) {
		        new PNotify({
		              title: 'Sukses',
		              text: 'Data berhasil disimpan',
		              type: 'success',
		              styling: 'bootstrap3',
		              delay: 2000
		          });
		        el.removeClass('btn-danger').removeClass('btn-success').addClass('btn-'+response.css_class).text(response.text).data('value', response.status);
		    })
		    .fail(function() {
		        new PNotify({
		              title: 'Gagal',
		              text: 'Data gagal disimpan',
		              type: 'error',
		              styling: 'bootstrap3'
		          });
		    })
		    
		    
		});

		$('#table-produk-master tbody').on( 'click', 'a.ajax-del-process', function (e) {
			var el = $(this);
			var id = $(this).data('id');
		    e.preventDefault();

		    $.ajax({
		    	url: $(this).attr('href'),
		    	type: 'POST',
		    	dataType: 'json',
		    	data: {	
		    			id: id,
		    			submit: 1
		    		},
		    })
		    .done(function(response) {
	    		t.ajax.reload();
		    })
		    .fail(function() {
		    	console.log("error");
		    })
		    .always(function() {
		    	console.log("complete");
		    });
		    
		});

		/** Edit Modal */
		$('#table-produk-master tbody').on( 'click', 'a.edit-modal-produk', function (e) {
			var el = $(this);
			var imgWrapper = function(name, id){
				return '<div class="img-wrap"> \
						<span class="close">&times;</span>\
					    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/images/master_produk/'+name+'" data-id="'+id+'">\
					    <input type="hidden" disabled="disabled" name="img_file[]" value="'+id+'">\
						</div>';
				};

		    e.preventDefault();

		    $.ajax({
		    	url: '<?php echo base_url(); ?>master/produk/edit',
		    	type: 'POST',
		    	dataType: 'json',
		    	data: {id: $(this).data('id')},
		    })
		    .done(function(response) {
		    	store.set('id', { name:response.id });
		    	var storeId = store.get('id').name;

		    	/** FineUploader Edit */
					$('#fine-uploader-manual-trigger-edit').fineUploader({
			            template: 'qq-template-manual-trigger',
			            request: {
			                endpoint: '<?php echo base_url(); ?>master/produk/edit',
			                params: {submit: 'image', produk_id: storeId}
			            },
			            validation: {
			            	minSizeLimit: 1000,
			            },
			            thumbnails: {
			                placeholders: {
			                    waitingPath: '<?php echo base_url(); ?>assets/images/waiting-generic.png',
			                    notAvailablePath: '<?php echo base_url(); ?>assets/images/not_available-generic.png'
			                }
			            },
			            autoUpload: false
			        });

				$('#edit-data-modal').on('show.bs.modal', function () {
				  	$('#edit-kode-produk').focus()

				  	/**Generate value */
				  		var radio = $('#edit-data-modal').find('.radio-status');
				  		$('#edit-data-modal').find('#edit-kode-produk').val(response.id_produk);
				  		$('#edit-data-modal').find('#edit-nama').val(response.nama);
				  		$('#edit-data-modal').find('#edit-deskripsi').text(response.deskripsi);
				  		$('#edit-data-modal').find('#edit-harga-satuan').val(response.harga_satuan);
				  		$('#edit-data-modal').find('#id-produk').val(response.id);

				  		radio.parent().removeClass('active');
				  		radio.removeAttr('checked');
				  		$('#edit-data-modal').find('.radio-status[value="'+response.status+'"]').attr('checked', true);
				  		$('#edit-data-modal').find('.radio-status[value="'+response.status+'"]').prop("checked", true);
				  		$('#edit-data-modal').find('.radio-status[value="'+response.status+'"]').parent().addClass('active');

						//destroy image-wrapper
						$('#edit-data-modal').on('hide.bs.modal', function(e){
							$('#edit-data-modal').find('#image_wrapper > .img-wrap').remove();
				  			$(this).off('hide.bs.modal');
						});

						for (var i = 0; i < response.image.length; i++) {
				  			$(imgWrapper(response.image[i].img, response.image[i].id)).appendTo('#image_wrapper');
				  		}

				  		$('#edit-data-modal').find('.img-wrap > .close').on('click', function(){
							$(this).parent().find('img').remove();
							$(this).parent().find('input[type="hidden"]').removeAttr('disabled');
							$(this).parent().css('border', 'none');
							$(this).parent().find('.close').remove();
						});

				  		//prevent for listening multiple
				  		$(this).off('show.bs.modal');

				});

				$('#edit-data-modal').modal({
		    		cache:false
		    	});

		    })
		    .fail(function() {
		    	console.log("error");
		    })
		    .always(function() {
		    	console.log("complete");
		    });
		    
		    
		});

		$('#form-edit-data-master button[type="reset"]').on('click', function(){
		    $('#fine-uploader-manual-trigger-edit').fineUploader('reset');
        });

		$('[type="reset"]').on('click', function(){
			$('textarea').text('');
		});


		$('#form-edit-data-master').on('submit', function(e){
        	
        	e.preventDefault();

	        var validateFront = function() {
	          if (true === $('#form-edit-data-master').parsley().isValid()) {
	            $('.bs-callout-info').removeClass('hidden');
	            $('.bs-callout-warning').addClass('hidden');

	            	//check whether there is uploaded file in fineuploader wrapper
				    	if($('#form-edit-data-master').find('ul.qq-upload-list li').length > 0){
				   		    $('#fine-uploader-manual-trigger-edit').fineUploader('uploadStoredFiles');
				   		}

				    //submit data produk
				    	$.ajax({
				    		url: '<?php echo base_url(); ?>master/produk/edit',
				    		type: 'POST',
				    		dataType: 'json',
				    		data: $('#form-edit-data-master').serialize(),
				    	})
				    	.done(function(response) {
				    		new PNotify({
		                                  title: 'Sukses',
		                                  text: 'Data berhasil diubah',
		                                  type: 'success',
		                                  styling: 'bootstrap3'
		                              });

				    		if(response.deleted_id != undefined){
				    			if(response.deleted_id.length > 0){
					    			$.each(response.deleted_id[0], function(index, val) {
						    			 /* iterate through array or object */
				    					$('#edit-data-modal').find('.img-wrap > input[value="'+val+'"]').remove();
						    		});
					    		}	
				    		}

				    		t.ajax.reload();

				    	})
				    	.fail(function() {
				    		console.log("error");
				    	})
				    	.always(function() {
				    		console.log("complete");
				    	});
	            
	            return true;
	           	
	          } else {

	            $('.bs-callout-info').addClass('hidden');
	            $('.bs-callout-warning').removeClass('hidden');

	            return false;
	          }
	        };

	        validateFront();
        });
	});
	
	
</script>

