<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		return $this->table = 'master_produk';

	}

	public function get_all(){
		$this->db->select('a.*, b.nama as satuan, b.simbol, a.status as produk_status');
		$this->db->from('master_produk a');
		$this->db->join('master_satuan b', 'b.id = a.satuan_id');
		$this->db->order_by('a.id','DESC');

		$q = $this->db->get();

		if($q->num_rows() > 0){
			return $q->result_array();
		}

		return false;
	}

	public function insert($data){
		return $this->add($data);
	}

	public function edit($conditions, $data){
		return $this->update($conditions, $data);
	}

	public function update_status($id, $data){
		$conditions = array('id' => $id);
		return $this->update($conditions, $data);
	}

	public function get_product_images($id){
		$this->db->select('');
		$this->db->from('master_produk a');
		$this->db->join('master_produk_image b','b.id_master_produk = a.id');
		$this->db->where('a.id',$id);
		$this->db->where('b.status','y');
		
		$q = $this->db->get();

		if($q->num_rows() > 0){
			return $q->result_array();
		}

		return false;
	}
}

/* End of file modelName.php */
/* Location: ./application/models/modelName.php */