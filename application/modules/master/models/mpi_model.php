<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MPI_Model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		return $this->table = 'master_produk_image';

	}

	public function get_all(){
		$order = array('id','desc');
		return $this->get_data($order);
	}

	public function insert($data){
		$data1 = array('id_coa' => $this->general->generate_number());
		$data = array_merge($data, $data1);
		return $this->add($data);
	}

	public function get_images($data){
		$this->db->select('img');
		$this->db->where_in('id', $data);
		$this->db->from($this->table);
		$q = $this->db->get();

		if($q->num_rows() > 0){
			return $q->result_array();
		}

		return false;
	}

	public function del_image($data){
		$this->db->where_in('id', $data);
		$this->db->delete($this->table);
	}

}

/* End of file coa_Model.php */
/* Location: ./application/models/coa_Model.php */