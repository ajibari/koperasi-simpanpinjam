<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satuan_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		return $this->table = 'master_satuan';
	}
	
	public function get_all(){
		$this->db->select('*');
		$this->db->where('a.status','y');
		$this->db->from('master_satuan a');
		$q = $this->db->get();

		if($q->num_rows() > 0){
			return $result = $q->result_array();
		}

		return false;
	}

}

/* End of file modelName.php */
/* Location: ./application/models/modelName.php */