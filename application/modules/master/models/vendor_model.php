<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor_Model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		return $this->table = 'master_vendor';

	}

	public function get_all(){
		$order = array('id','desc');
		return $this->get_data($order);
	}

	public function insert($data){
		$data1 = array('id_vendor' => $this->general->generate_number());
		$data = array_merge($data, $data1);
		return $this->add($data);
	}

	public function edit($conditions, $data){
		return $this->update($conditions, $data);
	}

	
}

/* End of file coa_Model.php */
/* Location: ./application/models/coa_Model.php */