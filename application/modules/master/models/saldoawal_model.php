<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saldoawal_Model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		return $this->table = 'master_saldoawal';

	}

	public function get_all(){
		$this->db->select('a.id, a.amount, a.status, b.kode, b.nama');
		$this->db->from('master_saldoawal a');
		$this->db->join('master_coa b','b.id = a.id_coa');
		$this->db->order_by('a.id','DESC');

		$q = $this->db->get();

		if($q->num_rows() > 0){
			return $q->result_array();
		}

		return false;
	}

	public function insert($data){
		return $this->add($data);
	}

	public function edit($conditions, $data){
		return $this->update($conditions, $data);
	}

	
}

/* End of file coa_Model.php */
/* Location: ./application/models/coa_Model.php */