<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coa_Model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		return $this->table = 'master_coa';

	}

	public function get_all(){
		$order = array('id','desc');
		return $this->get_data($order);
	}

	public function insert($data){
		$data1 = array('id_coa' => $this->general->generate_number());
		$data = array_merge($data, $data1);
		return $this->add($data);
	}

	public function edit($conditions, $data){
		return $this->update($conditions, $data);
	}

	public function autocomplete($conditions = null){
		$this->db->select('id, kode ,nama, id_coa');
		if(is_array($conditions)){
			foreach($conditions as $c => $v){
				$this->db->where($c,$v);
			}
		}

		$this->db->where('status','y'); //yang aktif saja
		$this->db->from($this->table);
		$q = $this->db->get();

		if($q->num_rows() > 0){
			return $q->result_array();
		}

		return false;
	}

}

/* End of file coa_Model.php */
/* Location: ./application/models/coa_Model.php */