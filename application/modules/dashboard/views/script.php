<?php 

$assets = base_url().'assets/plugins/';

?>

<script src="<?php echo $assets; ?>datatables/Buttons-1.4.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo $assets; ?>storejs/store.legacy.min.js"></script>
<script src="<?php echo $assets; ?>parsleyjs/dist/parsley.min.js"></script>
<script src="<?php echo $assets; ?>datatables/Buttons-1.4.2/js/buttons.flash.min.js"></script>
<script src="<?php echo $assets; ?>datatables/JSZip-2.5.0/jszip.min.js"></script>
<script src="<?php echo $assets; ?>datatables/pdfmake-0.1.32/pdfmake.min.js"></script>
<script src="<?php echo $assets; ?>datatables/pdfmake-0.1.32/vfs_fonts.js"></script>
<script src="<?php echo $assets; ?>datatables/Buttons-1.4.2/js/buttons.html5.min.js"></script>
<script src="<?php echo $assets; ?>datatables/Buttons-1.4.2/js/buttons.print.min.js"></script>
<script src="<?php echo $assets; ?>fine-uploader/js/jquery.fine-uploader.min.js"></script>
<script src="<?php echo $assets; ?>pnotify/dist/pnotify.js"></script>
<script src="<?php echo $assets; ?>jquery.tagsinput/src/jquery.tagsinput.js"></script>
<script src="<?php echo $assets; ?>jstepper/jquery.jstepper.js"></script>
<script src="<?php echo $assets; ?>select2/dist/js/select2.full.min.js"></script>

<!-- Custom -->
<script>
	var base_url = '<?php echo base_url(); ?>';
	var store;
</script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
