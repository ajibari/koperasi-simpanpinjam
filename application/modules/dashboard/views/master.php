<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php if(!empty($title)){ echo $title; } else {echo "Backend System";}?></title>
        
        <!-- link css -->

            <?php 
                if(!empty($link_css)){
                    echo $link_css;
                }
            ?>

        <!-- /link css -->
        <!-- jQuery -->
        <?php $assets = base_url().'assets/plugins/'; ?>
        <script src="<?php echo $assets; ?>jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo $assets; ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- Plugins -->
        <script src="<?php echo $assets; ?>datatables/datatables.min.js"></script>
    </head>

    <body class="<?php if(!empty($body_class)){ echo $body_class;} else { echo "nav-md"; }; ?>">
        
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left-col scroll-view">
                        <div class="navbar nav_title"></div>
                        <div class="clearfix"></div>
                        <div class="profile"></div>
                            <?php 
                            if(!empty($sidebar)){
                                echo $sidebar;
                            }
                            ?>
                    </div>
                </div>
                <div class="top_nav">

                <?php
                    if(!empty($nav_menu)){
                        echo $nav_menu;
                    }
                ?>

                </div>
    
                <?php 
                    if(!empty($content)){
                        echo $content;
                    }
                ?>

            </div>    
        </div>

    </body>

    <!-- footer content -->
        <?php 
            if(!empty($footer)){
                echo $footer;
            }
        ?>
    <!-- /footer content -->

    <!-- Javascript -->
        <?php 
            if(!empty($script)){
                echo $script;
            }
        ?>
</html>