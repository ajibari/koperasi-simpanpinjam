<?php 
$plugin = base_url().'assets/plugins/'; 
$css = base_url().'assets/css/';
?>


<!-- Bootstrap -->
<link href="<?php echo $plugin; ?>bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo $plugin; ?>datatables/datatables.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="<?php echo $plugin; ?>font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- Datatables -->
<link href="<?php echo $plugin; ?>datatables/datatables.css" rel="stylesheet">
<link href="<?php echo $plugin; ?>datatables/Buttons-1.4.2/css/buttons.dataTables.min.css" rel="stylesheet">
<!-- FineUploader -->
<link href="<?php echo $plugin; ?>fine-uploader/css/fine-uploader-new.css" rel="stylesheet">
<!-- Pnotify -->
<link href="<?php echo $plugin; ?>pnotify/dist/pnotify.css" rel="stylesheet">
<link href="<?php echo $plugin; ?>jquery.tagsinput/src/jquery.tagsinput.css" rel="stylesheet">
<link href="<?php echo $plugin; ?>select2/dist/css/select2.min.css" rel="stylesheet">

<!-- bootstrap-progressbar -->
<!-- <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"> -->
<!-- jVectorMap -->
<!-- <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/> -->

<!-- Custom Theme Style -->
<link href="<?php echo $css; ?>custom.css" rel="stylesheet">