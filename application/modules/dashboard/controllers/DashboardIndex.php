<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardIndex extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index(){
		$this->render('dashboard/dashboard_index');
	}

	public function _script(){
		$this->load->view('dashboard/script');
	}

	public function _header(){
        $data = $this->general->array_data();
		$this->load->view('dashboard/header', $data);
	}

	public function _nav_menu(){
		$data = array();
		$this->load->view('dashboard/nav_menu', $data);
	}

	public function _sidebar(){
        // $data = $this->general->array_data();
        $data = array();
		$this->load->view('dashboard/sidebar', $data);
	}

	public function _footer(){
		$this->load->view('dashboard/footer');
	}

	public function _link_css(){
		$this->load->view('dashboard/link_css');
	}

	public function json_data(){
		$result = array(
			array(
				'id' => 1, 
				'name' => 'Xander',
				'position' => 'Officer',
				'salary' => 129084,
				'start_date' => '19-02-1998',
				'office' => 'California',
				'extn' => 3231
			),
			array(
				'id' => 2, 
				'name' => 'Robert',
				'position' => 'Officer',
				'salary' => 129334,
				'start_date' => '19-03-1997',
				'office' => 'Texas',
				'extn' => 3331
			)
		);

		$json['data'] = $result;
		echo json_encode($json);
	}
}
