<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activitylog_Model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		return $this->table = 'activity_log';

	}

	public function insert($data){
		$this->db->insert($this->table, $data);

		return $this->db->affected_rows();
	}

}

/* End of file coa_Model.php */
/* Location: ./application/models/coa_Model.php */