<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	public $field_default_create;
	public $field_default_modify;
	public $table;

	public function __construct(){
		parent::__construct();

		$user_id = $this->session->userdata('logged_uid');
		$now = date('Y-m-d H:i:s');
		
		$this->field_default_create = array('date_created' => $now, 'user_created' => $user_id);
		$this->field_default_modify = array('date_modified' => $now, 'user_modified' => $user_id);
	}

	public function get_data($order_by){
		//from one table only
			$this->db->from($this->table);
			$this->db->order_by($order_by[0],$order_by[1]);
			$query = $this->db->get();

			if($query->num_rows() > 0){
				return $query->result_array();
			}

			return false;
	}

	public function get_by($condition){
		//from one table only with conditions
			foreach($condition as $c => $v){
				$this->db->where($c, $v);
			}
			$this->db->from($this->table);
			$query = $this->db->get();

			if($query->num_rows() > 0){
				return $query->row_array();
			}

			return false;
	}

	public function add($data){
		//add data to one table only
			$data = array_merge($this->field_default_create, $data);
			$this->db->insert($this->table, $data);
			return $this->db->affected_rows();
	}

	public function del_soft($conditions, $status){
		//change status to one table only
			foreach($conditions as $c => $v){
				$this->db->where($c, $v);
			}

		$query = $this->db->update($this->table, $status);
		return $this->db->affected_rows();
	}

	public function del($conditions){
		//delete data in one table only
			foreach($conditions as $c => $v){
				$this->db->where($c, $v);
			}

			$query = $this->db->delete($this->table);
			return true;
	}

	public function truncate(){
		//truncate one table only
		$this->db->empty_table($this->table);
	}

	public function update($conditions, $data){
		//update data in one table only
			foreach($conditions as $c => $v){
				$this->db->where($c, $v);
			}

			$data = array_merge($this->field_default_modify, $data);
			$query = $this->db->update($this->table, $data);
			return $this->db->affected_rows();
	}
}

/* End of file modelName.php */
/* Location: ./application/models/modelName.php */