<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	/**
	 * Hold boolean value whether script can continue executed.
	 *
	 * @var bool
	 */
	protected $pass = FALSE;

	/**
	 * Hold data to pass cross methods.
	 *
	 * @var array
	 */
	protected $data = array();
	
	/**
	 * Create controller instance, Check some stuff.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		// $this->general->is_js_disabled();
		$this->lang->load('message','indonesia');
		$this->authentication->is_login();
	}

	/**
	 * Render view. We'll check first if it's come from ajax, if yes,
	 * then return just desired part, render whole page otherwise.
	 *
	 * @param string $view
	 * @param $data
	 * @return void
	 */
	public function render($view, array $data = array())
	{
		$data['class'] 		= "page-header-fixed page-footer-fixed";
		$data['link_css']	= Modules::run('dashboard/DashboardIndex/_link_css');
		$data['header']		= Modules::run('dashboard/DashboardIndex/_header');
		$data['nav_menu'] 	= Modules::run('dashboard/DashboardIndex/_nav_menu');
		$data['footer']		= Modules::run('dashboard/DashboardIndex/_footer');
		$data['script']		= Modules::run('dashboard/DashboardIndex/_script');
		$data['sidebar']	= Modules::run('dashboard/DashboardIndex/_sidebar');
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			$this->load->view( $view, $data, FALSE);
		
		} else {
			$data['content']	= $this->load->view($view, $data, TRUE);
			$this->load->view('dashboard/master', $data, FALSE);
		}
	}


	/**
	 * Validation. This function take three argument which is first is user
	 * input, second is validation rules, and last, which is optioal, error
	 * delimiter.
	 *
	 *  rules must be an array, e,g:
	 *	$rules = array(
     *           array(
     *                'field' => 'code', 
     *                 'label' => 'Kode BPS', 
     *                 'rules' => 'required'
     *              ),
     *           array(
     *                 'field' => 'name', 
     *                 'label' => 'Nama Propinsi', 
     *                 'rules' => 'required'
     *              ),
     *           array(
     *           		'field' => 'status',
     *           		'label' => 'Status',
     *           		'rules' => 'required'
     *          	)
     *        );
     *
     * @param array $input
     * @param array $rules
     * @param array $error_delimiter
     * @return bool
    */
	public function validation(array $input, array $rules, array $error_delimiter = array())
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules($rules);

		if ($error_delimiter)
		{
			$this->form_validation->set_error_delimiters($error_delimiter[0], $error_delimiter[1]);
		}
		else
		{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger pad-3">', '</div>');
		}
		
		return $this->form_validation->run();
	}
}